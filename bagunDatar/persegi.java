package bagunDatar;

public class persegi extends bagunDatarPrn{
   
    protected double sisiPersegi;

    public double getSisiPersegi() {
        return sisiPersegi;
    }

    public void setSisiPersegi(double sisiPersegi) {
        this.sisiPersegi = sisiPersegi;
    }
    
    @Override
    public double getLuas() {
        return sisiPersegi*sisiPersegi;
    }
    
}
