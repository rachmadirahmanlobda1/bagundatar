package bagunDatar;

public class main {
    public static void main(String[]args) {
        segitiga stiga = new segitiga();
        stiga.setAlas(8);
        stiga.setTinggi(14);
        System.out.println("Luas Segitiga: "+ stiga.getLuas());
    
        lingkaran ling = new lingkaran();
        ling.setDiameterLingkaran(10);
        System.out.println("Luas lingkaran: " + ling.getLuas());
        
        persegi segi = new persegi();
        segi.setSisiPersegi(10);
        System.out.println("Luas Persegi "+ segi.getLuas());
        
        persegiPanjang pp = new persegiPanjang();
        pp.setPersegiPanjang(10);
        pp.setLebar(5);
        System.out.println("Luas Persegi Panjang "+ pp.getLuas());
    }

}
